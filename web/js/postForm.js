$(function () {

    $("#schedule-data").on("dblclick", "td", function (event) {
        var data = $(this).find('span').attr('id');

        postURL(ctx + "/editSchedule?" + data);


    });

    /**
     * Takes a URL and goes to it using the POST method.
     * @param {string} url  The URL with the GET parameters to go to.
     * @param {boolean=} multipart  Indicates that the data will be sent using the
     *     multipart enctype.
     */
    function postURL(url, multipart) {
        var form = document.createElement("FORM");
        form.method = "get";
        if (multipart) {
            form.enctype = "multipart/form-data";
        }
        form.style.display = "none";
        document.body.appendChild(form);
        form.action = url.replace(/\?(.*)/, function (_, urlArgs) {
            urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function (input, key, value) {
                input = document.createElement("INPUT");
                input.type = "hidden";
                input.name = decodeURIComponent(key);
                input.value = decodeURIComponent(value);
                form.appendChild(input);
            });
            return "";
        });
        form.submit();
    }


    // Attach a submit handler to the form
    $("#create_user").submit(function (event) {

        // Stop form from submitting normally
        event.preventDefault();
        $("#create-user-submit").prop('disabled', true);

        // Get some values from elements on the page:
        var $form = $(this);

        // We want to customize what we post, therefore we format our data
        var data = "user=create"
            + "&u=" + $('#create-username').val()
            + "&p=" + $('#create-newpass').val()
            + "&ul=" + $('#create-userLevel').val()
            + "&fname=" + $('#create-fName').val()
            + "&lname=" + $('#create-lName').val()
            + "&token=" + $('#token').val();

        // For debugging purposes... see your console:
//        console.log(data);

        // The actual from POST method
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: data,
            success: function (data) {
                //console.log("reply ");
                //console.log(data);
                if (data.createUser == true) {

                    document.getElementById("error").innerHTML = "User created!";

                }
                else {
                    JSON.stringify(data.createUser);

                    document.getElementById("error").innerHTML = "User could not be created! " + "Error: " + JSON.stringify(data.createUser.detailMessage);
                    $("#create-user-submit").prop('disabled', false);

                }


            }

        });
    });
    // Attach a submit handler to the form
    $("#edit_schedule").submit(function (event) {

        // Stop form from submitting normally
        event.preventDefault();
        $("#edit_schedule_submit").prop('disabled', true);

        // Get some values from elements on the page:
        var $form = $(this);

        // We want to customize what we post, therefore we format our data
        var data = "schedule=" + + $('#edit-status').val()
            + "&sid=" + $('#edit-sid').val()
            + "&uid=" + $('#edit-user-id').val()
            + "&date=" + $('#edit-date').val()
            + "&time-from=" + $('#edit-time-from').val()
            + "&time-to=" + $('#edit-time-to').val()
            + "&time-check-in=" + $('#edit-time-check-in').val()
            + "&time-check-out=" + $('#edit-time-check-out').val()
            + "&token=" + $('#token').val();

        // For debugging purposes... see your console:
       //console.log(data);

        // The actual from POST method
        $.ajax({
            type: $form.attr('method'),
            url: $form.attr('action'),
            data: data,
            success: function (data) {
                //console.log("reply ");
                //console.log(data);
                if (data.createSchedule == true) {

                    document.getElementById("error").innerHTML = "Schedule created!";

                }
                else {
                    JSON.stringify(data.createSchedule);

                    document.getElementById("error").innerHTML = "Schedule could not be created! " + "Error: " + JSON.stringify(data.createSchedule.detailMessage);
                    $("#edit_schedule_submit").prop('disabled', false);

                }


            }

        });
    });

    // Attach a submit handler to the form
    $("#edit_user_0").submit(function (event) {

        // Stop form from submitting normally
        event.preventDefault();
        $("#edit_user_submit").prop('disabled', true);

        var isFormValid = true;

        $("#edit_user_0 input").each(function () {
            if ($.trim($(this).val()).length == 0) {

                if (($(this).id == $('#edit-newpass-confirm').id) || ($(this).id == $('#edit-newpass').id)) {
                }
                else {
                    $(this).addClass("w3-border-red w3-text-red");
                    isFormValid = false;
                }
            }
            else {
                $(this).removeClass("w3-border-red w3-text-red");
            }
        });

        if (!isFormValid) {
            $("#error").html("Please fill in all the required fields .");
            $("#edit_user_submit").prop('disabled', false);

            return;
        }
        else {
            if (!checkPasswordMatch()) {
                $("#edit_user_submit").prop('disabled', false);

                return;
            }
            else {
                // Get some values from elements on the page:
                var $form = $(this);
                var confirmPassword = $("#edit-newpass-confirm").val();

                // We want to customize what we post, therefore we format our data
                var data = "user=update"
                    + "&uid=" + $('#edit-uid').val()
                    + "&u=" + $('#edit-username').val()
                    + "&ul=" + $('#edit-userLevel').val()
                    + "&fname=" + $('#edit-fName').val()
                    + "&lname=" + $('#edit-lName').val()
                    + "&token=" + $('#token').val();

               // console.log("length = " + confirmPassword.length);
                if (checkPasswordMatch() && confirmPassword.length > 0) {
                    data = data + "&cp=1&p=" + confirmPassword;
                }
                else if (confirmPassword.length == 0) {
                    date = data + "&cp=0"
                }
                else {
                    $("#edit_user_submit").prop('disabled', false);

                    return;
                }

                // For debugging purposes... see your console:
                //console.log(data);

                // The actual from POST method
                $.ajax({
                    type: $form.attr('method'),
                    url: $form.attr('action'),
                    data: data,
                    success: function (data) {
                        //console.log("reply ");
                        //  console.log(data);
                        if (data.editUser == true) {

                            document.getElementById("error").innerHTML = "User Updated!";

                        }
                        else {
                            JSON.stringify(data.editUser);

                            document.getElementById("error").innerHTML = "User could not be created! " + "Error: " + JSON.stringify(data.editUser.detailMessage);

                        }
                        $("#edit_user_submit").prop('disabled', false);


                    }

                });
            }
        }


    });


});

// Attach a submit handler to the form
$("#edit_user_1").submit(function (event) {

    // Stop form from submitting normally
    event.preventDefault();
    $("#edit_user_submit").prop('disabled', true);

    var isFormValid = true;

    $("#edit_user_1 input").each(function () {
        if ($.trim($(this).val()).length == 0) {
            if ($(this) == $('#edit-newpass-confirm') || $(this) == $('#edit-newpass') || $(this) == $('#edit-oldpass')) {
            }
            else {
            $(this).addClass("w3-border-red w3-text-red");
            isFormValid = false;
        }
        }
        else {
            $(this).removeClass("w3-border-red w3-text-red");
        }
    });

    if (!isFormValid) {
        $("#error").html("Please fill in all the required fields .");
        $("#edit_user_submit").prop('disabled', false);
        return;
    }
    else {

        var confirmPassword = $("#edit-newpass-confirm").val();


            // Get some values from elements on the page:
            var $form = $(this);

            // We want to customize what we post, therefore we format our data
            var data = "user=update"
                + "&uid=" + $('#edit-uid').val()
                + "&u=" + $('#edit-username').val()
                + "&fname=" + $('#edit-fName').val()
                + "&lname=" + $('#edit-lName').val()
                + "&token=" + $('#token').val()
                + "&ul=" + $('#edit-userLevel').val();


            if (!checkOldPassword() && checkPasswordMatch()) {
                data = data + "&cp=1&oldp=" + $('#edit-oldpass').val()
                    + "&p=" + confirmPassword;
            }
            else if (confirmPassword.length == 0) {
                date = data + "&cp=0"
            }
            else {
                $("#edit_user_submit").prop('disabled', false);
                return;
            }

            // For debugging purposes... see your console:
            // console.log(data);

            // The actual from POST method
            $.ajax({
                type: $form.attr('method'),
                url: $form.attr('action'),
                data: data,
                success: function (data) {
                    //console.log("reply ");
                    //  console.log(data);
                    if (data.editUser == true) {

                        document.getElementById("error").innerHTML = "User Updated!";

                    }
                    else {
                        JSON.stringify(data.editUser);

                        document.getElementById("error").innerHTML = "User could not be created! " + "Error: " + JSON.stringify(data.editUser.detailMessage);

                    }
                    $("#edit_user_submit").prop('disabled', false);


                }

            });
        }



});


function checkOldPassword() {
    var empty = false;
    var oldPassword = $("#edit-oldpass").val();

    if (oldPassword.length > 0) {
        empty = false;
        $("#error").html("Old password entered!");
    }
    else {
        empty = true;
        $("#error").html("Enter old password.");

    }
    return empty;
}

function checkPasswordMatch() {
    var match = false;
    var password = $("#edit-newpass").val();
    var confirmPassword = $("#edit-newpass-confirm").val();

    if (password != confirmPassword) {
        match = false;
        $("#error").html("Passwords do not match!");
        $("#edit_user_submit").prop('disabled', true);
    }
    else {
        match = true;
        $("#error").html("Passwords match.");
        $("#edit_user_submit").prop('disabled', false);

    }
    return match;
}

