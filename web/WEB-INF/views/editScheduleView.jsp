<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis - Edit schedule</title>
    <meta name="description" content="Edit Schedule">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>


</head>

<body>
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">
    <ul class="navbar">
        <li class=""><a href="${pageContext.request.contextPath}/createUser"
                        class="btn btn-primary btn-md"><span>New user&nbsp;<i
                class="glyphicon glyphicon-user"></i></span></a>
        </li>
        <li class="w3-hover-opacity"><a href="${pageContext.request.contextPath}/editSchedule"
                                        class="btn btn-primary btn-md"><span>New Schedule&nbsp;<i
                class="glyphicon glyphicon-calendar"></i></span></a>

        </li>
    </ul>
</div>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-3 ">

            <div class=" w3-center w3-light-grey ">
                <h1>Schedule</h1>
                <h3>Edit and Create</h3>
            </div>
            <div class="">
                <table class="w3-table" style="width: 100%;">
                    <thead>
                    <th style="width: 30%;"></th>
                    <th style="width: 70%;"></th>


                    </thead>
                    <tbody>
                    <form id="edit_schedule" class="" action="${pageContext.request.contextPath}/restricted"
                          method="GET">
                        <input type="hidden" name="schedule" value="${scheduleStatus}" id="edit-status">
                        <input type="hidden" name="token" value="${userLoggedIn.TOKEN}" id="token">
                        <input type="hidden" name="sid" value="${editSchedule.ID }" id="edit-sid">

                        <tr>
                            <td class="w3-container">

                                <div><label class="w3-label w3-text-black" for="edit-user-id"><i
                                        class="glyphicon glyphicon-user"></i></label>
                                    <input required readonly type="text" class="w3-input" value="${editSchedule.userID}"
                                           placeholder="User ID" name="uid"
                                           id="edit-user-id"></div>
                            </td>

                            <td class="w3-container">
                                <div><label class="w3-label w3-text-black" for="edit-date">Work
                                    Date <i
                                            class="glyphicon glyphicon-calendar"></i></label>
                                    <input required type="date" class="w3-input" value="${editSchedule.workDate}"
                                           placeholder="Date" name="date"
                                           id="edit-date"></div>
                            </td>

                        </tr>

                        <tr>
                            <td class="w3-container">
                                <div><label class="w3-label w3-text-black"
                                            for="edit-time-from">Start time <i
                                        class="glyphicon glyphicon-clock"></i></label>
                                    <input required type="time" class="w3-input" value="${editSchedule.startWorkTime}"
                                           placeholder="From"
                                           name="time-from"
                                           id="edit-time-from"></div>
                            </td>
                            <td class="w3-container">
                                <div><label class="w3-label w3-text-black"
                                            for="edit-time-check-in">Actual start <i
                                        class="fa fa-clock-o"></i></label>
                                    <input type="time" class="w3-input" value="00:00" placeholder="Check-In"
                                           name="time-check-in"
                                           id="edit-time-check-in"></div>
                            </td>
                        </tr>
                        <tr>

                            <td class="w3-container">
                                <div><label class="w3-label w3-text-black" for="edit-time-to">End
                                    time <i
                                            class="glyphicon glyphicon-clock"></i></label>
                                    <input required type="time" class="w3-input" value="${editSchedule.endWorkTime}"
                                           placeholder="To"
                                           name="time-to"
                                           id="edit-time-to"></div>
                            </td>
                            <td class="w3-container">
                                <div><label class="w3-label w3-text-black"
                                            for="edit-time-check-out">Actual end <i
                                        class="fa fa-clock-o"></i></label>
                                    <input type="time" class="w3-input" value="00:00" placeholder="Check-Out"
                                           name="time-check-out"
                                           id="edit-time-check-out"></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="w3-container">
                            </td>
                            <td class="w3-container">
                                <input id="edit_schedule_submit" class="btn btn-primary btn-md"
                                       type="submit"
                                       value="Save">    
                            </td>
                        </tr>


                        <tr>
                            <td colspan="2">
                                <p id="error" class="text-danger" >${errorString}</p>
                            </td>
                        </tr>

                    </form>
                    </tbody>
                </table>
                <script src="${pageContext.request.contextPath}/js/postForm.js"></script>
            </div>

        </div>

        <div class="col-sm-9 text-left">


        </div>
    </div>

    <footer class="navbar navbar-fixed-bottom navbar-inverse container-fluid  text-center">
        <p>Acernis Team</p>
    </footer>
</body>
</html>
