<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis - Terms</title>
    <meta name="description" content="Terms">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>


</head>

<body>
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">

</div>

<div class="container-fluid text-center">
    <div class="row content">


        </div>
        <div class="col-sm-9 text-left">

            <jsp:include page="docs/terms.html"/>

        </div>
    </div>
</div>


</body>
</html>