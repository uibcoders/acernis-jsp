<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<html>
<head>
    <title>Acernis - Login </title>
    <meta name="description" content="Login">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">



</head>

<body>


<div class="container">
    <div class="wrapper">
        <form action="${pageContext.request.contextPath}/dologin" method="post" name="Login_Form" class="form-login" style="max-width: 400px; margin: 0 auto;">
            <input type="hidden" name="caller" value="${pageContext.request.getParameter("caller")}">

            <div class="row text-center"></div>
            <h3 class="form-group text-center">
                <img src="" alt="Acernis"/>
            </h3>
            <hr class="">
            <div class="input-group">
                        <span class="input-group-addon" id="user-addon">
                            <i class="glyphicon glyphicon-user"></i>
                        </span>
                <input type="text" class="form-control" name="u" placeholder="Username" required autofocus/>
            </div>
            <div class="input-group">
                        <span class="input-group-addon" id="password-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                        </span>
                <input type="password" class="form-control" name="p" placeholder="Password" required/>
            </div>
            <button class="btn btn-lg btn-primary btn-block"  name="Submit" value="Login" type="Submit">Login</button>
        </form>

        <div style="max-width: 400px; margin: 0 auto;"><p id="error" class="" style="color: red;">${errorString}</p></div>
        </div>
    </div>

</body>
</html>
