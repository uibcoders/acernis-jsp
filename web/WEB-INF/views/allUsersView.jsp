<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis - Users</title>
    <meta name="description" content="All users">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script src="${pageContext.request.contextPath}/js/jquery.tablesorter.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.tablesorter.pager.js"></script>
    <script src="${pageContext.request.contextPath}/js/jquery.tablesorter.widgets.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>


</head>

<body class="">
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">
    <ul class="navbar">
        <li class=""><a href="${pageContext.request.contextPath}/createUser"
                        class="btn btn-primary btn-md"><span>New user&nbsp;<i
                class="glyphicon glyphicon-user"></i></span></a>
        </li>
        <li class="w3-hover-opacity"><a href="${pageContext.request.contextPath}/editSchedule"
                                        class="btn btn-primary btn-md"><span>New Schedule&nbsp;<i
                class="glyphicon glyphicon-calendar"></i></span></a>

        </li>
    </ul>
</div>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-3 ">


        </div>
        <div class="col-sm-9 text-left">


            <div class="pager tablesorter-pager">
                <span class="pagedisplay"></span>
            </div>


            <table id="tableAllUsers" class="tablesorter">
                <caption><h3>Users</h3></caption>
                <tfoot>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th> Level</th>
                    <th>ID</th>
                </tr>
                <tr>
                    <th colspan="7" class="ts-pager form-horizontal">
                        <button type="button" class="btn first"><i class="icon-step-backward glyphicon glyphicon-step-backward"></i></button>
                        <button type="button" class="btn prev"><i class="icon-arrow-left glyphicon glyphicon-backward"></i></button>
                        <span class="pagedisplay"></span> <!-- this can be any element, including an input -->
                        <button type="button" class="btn next"><i class="icon-arrow-right glyphicon glyphicon-forward"></i></button>
                        <button type="button" class="btn last"><i class="icon-step-forward glyphicon glyphicon-step-forward"></i></button>
                        <select class="pagesize input-mini" title="Select page size">
                            <option selected="selected" value="10">10</option>
                            <option value="20">20</option>
                            <option value="30">30</option>
                            <option value="40">40</option>
                        </select>
                        <select class="pagenum input-mini" title="Select page number"></select>
                    </th>
                </tr>
                </tfoot>
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Username</th>
                    <th> Level</th>
                    <th>ID</th>
                </tr>

                </thead>

                <tbody>


                <c:forEach var="u" items="${userList}">
                    <tr>
                        <td>
                        <span><a
                                href="${pageContext.request.contextPath}/editUser?uid=${u.ID} ">${u.fName} ${u.lName} </a></span>

                        <td>
                                ${u.userName} </td>
                        <td>
                                ${u.userLevel}</td>
                        <td>
                                ${u.ID} </td>
                    </tr>

                </c:forEach>

                </tbody>
            </table>

        </div>

    </div>
            <script src="${pageContext.request.contextPath}/js/postForm.js"></script>

    <script type="text/javascript">
        $(function() {

            // NOTE: $.tablesorter.theme.bootstrap is ALREADY INCLUDED in the jquery.tablesorter.widgets.js
            // file; it is included here to show how you can modify the default classes
            $.tablesorter.themes.bootstrap = {
                // these classes are added to the table. To see other table classes available,
                // look here: http://getbootstrap.com/css/#tables
                table        : 'table table-bordered table-striped',
                caption      : 'caption',
                // header class names
                header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
                sortNone     : '',
                sortAsc      : '',
                sortDesc     : '',
                active       : '', // applied when column is sorted
                hover        : '', // custom css required - a defined bootstrap style may not override other classes
                // icon class names
                icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
                iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
                iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
                iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
                filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
                footerRow    : '',
                footerCells  : '',
                even         : '', // even row zebra striping
                odd          : ''  // odd row zebra striping
            };

            // call the tablesorter plugin and apply the uitheme widget
            $("#tableAllUsers").tablesorter({
                // this will apply the bootstrap theme if "uitheme" widget is included
                // the widgetOptions.uitheme is no longer required to be set
                theme : "bootstrap",

                widthFixed: true,

                headerTemplate : '{content} {icon}', // new in v2.7. Needed to add the bootstrap icon!

                // widget code contained in the jquery.tablesorter.widgets.js file
                // use the zebra stripe widget if you plan on hiding any rows (filter widget)
                widgets : [ "uitheme", "filter", "zebra" ],

                widgetOptions : {
                    // using the default zebra striping class name, so it actually isn't included in the theme variable above
                    // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
                    zebra : ["even", "odd"],

                    // reset filters button
                    filter_reset : ".reset",

                    // extra css class name (string or array) added to the filter element (input or select)
                    filter_cssFilter: "form-control",

                    // set the uitheme widget to use the bootstrap theme class names
                    // this is no longer required, if theme is set
                    // ,uitheme : "bootstrap"

                }
            })
                    .tablesorterPager({

                        // target the pager markup - see the HTML block below
                        container: $(".ts-pager"),

                        // target the pager page select dropdown - choose a page
                        cssGoto  : ".pagenum",

                        // remove rows from the table to speed up the sort of large tables.
                        // setting this to false, only hides the non-visible rows; needed if you plan to add/remove rows with the pager enabled.
                        removeRows: false,

                        // output string - default is '{page}/{totalPages}';
                        // possible variables: {page}, {totalPages}, {filteredPages}, {startRow}, {endRow}, {filteredRows} and {totalRows}
                        output: '{startRow} - {endRow} / {filteredRows} ({totalRows})'

                    });

        });
    </script>
        </div>



</body>
</html>

