<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis - Edit user</title>
    <meta name="description" content="Edit user">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>



</head>

<body>
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">
    <ul class="navbar">
        <li class=""><a href="${pageContext.request.contextPath}/createUser"
                        class="btn btn-primary btn-md"><span>New user&nbsp;<i
                class="glyphicon glyphicon-user"></i></span></a>
        </li>
        <li class="w3-hover-opacity"><a href="${pageContext.request.contextPath}/editSchedule"
                                        class="btn btn-primary btn-md"><span>New Schedule&nbsp;<i
                class="glyphicon glyphicon-calendar"></i></span></a>

        </li>
    </ul>
</div>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-3 ">

            <c:choose>
                <c:when test="${userLoggedIn.userLevel >= '2' }">
                <form id="edit_user_0" action="${pageContext.request.contextPath}/restricted"
                      method="POST"
                      style="width: 250px;">
                </c:when>
                <c:otherwise>
                <form id="edit_user_1" action="${pageContext.request.contextPath}/restricted"
                      method="POST"
                      style="width: 250px;">
                </c:otherwise>
            </c:choose>



                <input type="hidden" name="token" value="${userLoggedIn.TOKEN}" id="token">
                <input type="hidden" name="uid" value="${editUser.ID }" id="edit-uid">


                <div class="form-group">
                    <label class="label"  for="edit-username"><i
                        class="fa fa-user"></i></label>
                    <input readonly type="text" class="form-control" value="${editUser.userName}"
                           placeholder="Username" name="u"
                           id="edit-username">
                </div>

                <div class="form-group">
                    <label class="label" for="edit-fName">First
                    name</label>
                    <input type="text" class="form-control" value="${editUser.fName}" placeholder="First Name"
                           name="fName"
                           id="edit-fName">
                </div>
                <div class="form-group">
                    <label class="label"   for="edit-lName">Last
                    name</label>
                    <input type="text" class="form-control" value="${editUser.lName}" placeholder="Last Name"
                           name="lName"
                           id="edit-lName">
                </div>
                <c:choose>
                    <c:when test="${userLoggedIn.userLevel >= '2' }">
                        <div class="form-group">
                            <label class="label"  for="edit-userLevel">Userlevel</label>

                            <input type="number" class="form-control" name="ul" value="${editUser.userLevel}" id="edit-userLevel">
                        </div>
                    </c:when>
                    <c:otherwise>
                        <input type="hidden" name="ul" value="${editUser.userLevel }" id="edit-userLevel">

                        <div class="form-group">
                            <label class="label"   for="edit-oldpass"><i
                                class="glyphicons glyphicons-keys"></i></label>

                            <input type="password" class="form-control" value="" placeholder="Old password" name="oldp"
                                   id="edit-oldpass">
                        </div>
                    </c:otherwise>
                </c:choose>


                <div class="form-group">
                    <label class="label"  for="edit-newpass"><i
                        class="glyphicons glyphicons-keys"></i></label>

                    <input type="password" class="form-control" value="" placeholder="New password" name="np"
                           id="edit-newpass">
                </div>


                <div class="form-group">
                    <label class="label"   for="edit-newpass-confirm"><i
                        class="glyphicons glyphicons-keys"></i></label>
                    <input type="password" class="form-control" value="" placeholder="Confirm password" name="npc"
                           id="edit-newpass-confirm" onChange="checkPasswordMatch();">
                </div>

                <input id="edit_user_submit" class="btn btn-primary btn-md" type="submit"
                       value="Save">    

                <p id="error" class="text-danger" >${errorString}</p>


            </form>


        </div>
        <div class="col-sm-9 text-left">

                  ${htmlCalendar}

        </div>
    </div>
</div>


<script src="${pageContext.request.contextPath}/js/postForm.js"></script>

</body>
</html>