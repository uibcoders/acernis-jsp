<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/styles.css">






<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Acernis</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li><a href="${pageContext.request.contextPath}">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Control
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="${pageContext.request.contextPath}/schedule"><i
                                class="fa fa-calendar w3-large"></i>&nbsp;Schedule</a></li>
                        <li><a href="${pageContext.request.contextPath}/users"><i class="fa fa-users w3-large"></i>&nbsp;Users</a>
                        </li>
                        <li><a href="#">Page 3</a></li>
                    </ul>
                </li>
                <li><a href="${pageContext.request.contextPath}/docs">Documentation</a></li>
                <li><a href="${pageContext.request.contextPath}/terms">Terms</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="${pageContext.request.contextPath}/editUser?uid=${userLoggedIn.ID}">Logged in as:
                    <b>${userLoggedIn.userName}</b>.&nbsp;ID: <b>${userLoggedIn.ID} </b><i class="fa fa-pencil"></i></a>
                </li>
                <li><a class="" href="${pageContext.request.contextPath}/logout"><span>Logout&nbsp;<i
                        class="glyphicon glyphicon-log-out"></i></span></a></li>
            </ul>
        </div>
    </div>
</nav>