<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis - Create user</title>
    <meta name="description" content="Create user">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>

</head>

<body>
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">
    <ul class="navbar">
        <li class=""><a href="${pageContext.request.contextPath}/createUser"
                        class="btn btn-primary btn-md"><span>New user&nbsp;<i
                class="glyphicon glyphicon-user"></i></span></a>
        </li>
        <li class="w3-hover-opacity"><a href="${pageContext.request.contextPath}/editSchedule"
                                        class="btn btn-primary btn-md"><span>New Schedule&nbsp;<i
                class="glyphicon glyphicon-calendar"></i></span></a>

        </li>
    </ul>
</div>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-3 ">

        <div class="w3-center  w3-light-grey">
            <h1>New User</h1>
        </div>
        <div class="container">
            <form id="create_user" class="" action="${pageContext.request.contextPath}/restricted" method="POST"
                  style="width: 250px;">

                <input type="hidden" name="token" value="${userLoggedIn.TOKEN}" id="token">
                <input type="hidden" name="ul" value="${userLoggedIn.userLevel -1 }" id="create-userLevel">



                       <div class="form-group"><label class="label"  for="create-username"><i class="glyphicon glyphicon-user"></i></label>
                        <input type="text" class="form-control" value="${editUser.userName}"
                               placeholder="Username" name="u"
                               id="create-username"></div>

                       <div class="form-group"><label class="label"  for="create-fName">First name</label>
                        <input type="text" class="form-control" value="${editUser.fName}" placeholder="First Name"
                               name="fName"
                               id="create-fName"></div>

                       <div class="form-group"><label class="label"  for="create-lName">Last name</label>
                        <input type="text" class="form-control" value="${editUser.lName}" placeholder="Last Name"
                               name="lName"
                               id="create-lName"></div>

                       <div class="form-group"><label class="label"  for="create-newpass"><i class="fa fa-key"></i></label>
                        <input type="password" class="form-control" value="" placeholder="Password" name="p"
                               id="create-newpass"></div>

                       <div class="form-group"><label class="label"  for="create-newpass-confirm"><i class="fa fa-key"></i></label>
                        <input type="password" class="form-control" value="" placeholder="Confirm password" name="pc"
                               id="create-newpass-confirm"></div>

                    <input id="create-user-submit" class="btn btn-primary btn-md" type="submit" value="Create">    

                    <p id="error" class="text-danger" >${errorString}</p>


            </form>
        </div>

        </div>
        <div class="col-sm-9 text-left">

    </div>
</div>
    </div>

<script src="${pageContext.request.contextPath}/js/postForm.js"></script>



</div>
</body>

</html>