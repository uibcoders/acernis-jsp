
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<!doctype html>

<html lang="en">
<head>

    <title>Acernis-Users</title>
    <meta name="description" content="Create users">
    <meta name="author" content="Acernis Team">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/theme.bootstrap.css">

    <script src="${pageContext.request.contextPath}/js/jquery-3.1.0.js"></script>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>

    <script>var ctx = "${pageContext.request.contextPath}"</script>

</head>

<body>
<div>
    <jsp:include page="includes/_menu.jsp"/>
</div>

<div class="container">
    <ul class="navbar">
        <li class=""><a href="${pageContext.request.contextPath}/createUser"
                        class="btn btn-primary btn-md"><span>New user&nbsp;<i
                class="glyphicon glyphicon-user"></i></span></a>
        </li>
        <li class="w3-hover-opacity"><a href="${pageContext.request.contextPath}/editSchedule"
                                        class="btn btn-primary btn-md"><span>New Schedule&nbsp;<i
                class="glyphicon glyphicon-calendar"></i></span></a>

        </li>
    </ul>
</div>

<div class="container-fluid text-center">
    <div class="row content">
        <div class="col-sm-3 ">
            <div>
                <h1>Edit User</h1>
            </div>
            <div class="">
                <c:choose>
                    <c:when test="${userLoggedIn.userLevel >= '2' }">
                    <form id="edit_user_0" class="" action="${pageContext.request.contextPath}/restricted" method="POST"
                          style="width: 250px;">
                    </c:when>
                    <c:otherwise>
                        <form id="edit_user_1" class="" action="${pageContext.request.contextPath}/db" method="POST"
                              style="width: 250px;">
                    </c:otherwise>
                </c:choose>


                    <input type="hidden" name="token" value="${userLoggedIn.TOKEN}" id="token">
                    <input type="hidden" name="uid" value="${editUser.ID }" id="edit-uid">


                    <div class="w3-card-12 w3-center" style="padding: 20px; border-radius: 5px;">

                        <div class="form-group">
                            <input disabled type="text" class="form-control" value="${editUser.userName}"
                                   placeholder="Username" name="u"
                                   id="edit-username">
                            <label class="label" for="edit-username"></label>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" value="${editUser.fName}" placeholder="First Name"
                                   name="fName"
                                   id="edit-fName">
                            <label class="label" for="edit-fName"></label>
                        </div>

                        <div class="form-group">
                            <input type="text" class="form-control" value="${editUser.lName}" placeholder="Last Name"
                                   name="lName"
                                   id="edit-lName">
                            <label class="label" for="edit-lName"></label>
                        </div>
                        <c:choose>
                            <c:when test="${userLoggedIn.userLevel >= '2' }">
                                <input type="number" name="ul" value="${editUser.userLevel }" id="edit-userLevel">

                            </c:when>
                            <c:otherwise>
                                <input type="hidden" name="ul" value="${editUser.userLevel }" id="edit-userLevel">

                                <div class="form-group">
                                    <input type="password" class="form-control" value="" placeholder="Old password" name="p"
                                           id="edit-pass">
                                    <label class="label" for="edit-pass"></label>
                                </div>
                            </c:otherwise>
                        </c:choose>


                        <div class="form-group">
                            <input type="password" class="form-control" value="" placeholder="New password" name="np"
                                   id="edit-newpass">
                            <label class="label" for="edit-newpass"></label>
                        </div>

                        <div class="form-group">
                            <input type="password" class="form-control" value="" placeholder="Confirm password" name="npc"
                                   id="edit-newpass-confirm" onChange="checkPasswordMatch();">
                            <label class="form-control" for="edit-newpass-confirm"></label>
                        </div>

                        <input id="edit_user_submit" class="w3-btn w3-ripple w3-blue w3-margin" type="submit"
                               value="Save">    

                        <p id="error" class="text-danger">${errorString}</p>


                    </div>
                </form>
                <script src="${pageContext.request.contextPath}/js/postForm.js"></script>
            </div>

        </div>
        <div class="col-sm-9 text-left">

            ${htmlCalendar}

        </div>
    </div>
</div>



</body>
</html>