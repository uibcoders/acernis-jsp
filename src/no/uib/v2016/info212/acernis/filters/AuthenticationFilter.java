package no.uib.v2016.info212.acernis.filters;

import no.uib.v2016.info212.acernis.util.MyUtils;
import org.apache.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@WebFilter(filterName = "authenticationFilter", urlPatterns = {"/*"})
public class AuthenticationFilter implements Filter {

    private Logger logger = Logger.getLogger(AuthenticationFilter.class);

    public void init(FilterConfig fConfig) throws ServletException {
        logger.info("AuthenticationFilter initialized");
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
        res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
        res.setDateHeader("Expires", 0); // Proxies.


        String uri = req.getRequestURI(); //get the requesting uri
        try {
            String query = req.getQueryString();//get the querystring from the request if exist
            if (query.length() > 0) {
                uri += "?" + query;
            }
        } catch (NullPointerException e) {

        }

        logger.info("Requested Resource::" + uri);

        HttpSession session = req.getSession(false);
        String loginURI = req.getContextPath() + "/login";
        String loginURI2 = req.getContextPath() + "/dologin";
        String css = req.getContextPath() + "/css/";
        String fonts = req.getContextPath() + "/fonts/";

        //     String restricted = req.getContextPath() + "/restricted";
        String db = req.getContextPath() + "/db";
        String restr = req.getContextPath() + "/restricted";


        boolean loggedIn = session != null && MyUtils.getUserLoggedIn(session) != null;
        boolean loginRequest = req.getRequestURI().equals(loginURI);
        boolean loginRequest2 = req.getRequestURI().equals(loginURI2);
        // boolean restrictedRequest = req.getRequestURI().equals(restricted);
        boolean dbdRequest = req.getRequestURI().equals(db);
        boolean restrictedRequest = req.getRequestURI().equals(restr);
        boolean cssRequest = req.getRequestURI().startsWith(css);
        boolean fontRequest = req.getRequestURI().startsWith(fonts);


        if (loggedIn || loginRequest || loginRequest2 || dbdRequest || cssRequest || fontRequest || restrictedRequest) { //allowed
            // pass the request along the filter chain
            chain.doFilter(request, response);
        } else {
            logger.error("Unauthorized access request");
            String callerStr = "";
            if (uri.contains(req.getContextPath()) && !uri.equals(req.getContextPath()+"/"))
            {
                 callerStr = "?caller=" + Base64.getEncoder().encodeToString(uri.getBytes(StandardCharsets.UTF_8));

            }
            res.sendRedirect(req.getContextPath() + "/login" + callerStr);
        }


    }

    public void destroy() {
        //close any resources here
    }

}