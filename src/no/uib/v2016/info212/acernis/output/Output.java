package no.uib.v2016.info212.acernis.output;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.io.PrintWriter;

/**
 * Created by stian on 15.09.2016.
 */
public class Output {


    /**
     * Prints a valid response json, with the object data
     *
     * @param out The current html page writer
     * @param obj The object to add to the json object
     */
    public static void printValid(PrintWriter out, String type, Object obj) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement userObj = gson.toJsonTree(obj);

        //create a new JSON object
        JsonObject myObj = new JsonObject();
        //add property as success
        myObj.addProperty("success", true);
        //add the user object
        myObj.add(type, userObj);
        //convert the JSON to string and send back
        out.println(gson.toJson(myObj));
    }

    /**
     * Prints a false success json object
     *
     * @param out The current html page writer
     */
    public static void printInvalid(PrintWriter out, String type, Object obj) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement userObj = gson.toJsonTree(obj);

        //create a new JSON object
        JsonObject myObj = new JsonObject();
        //add property as success
        myObj.addProperty("success", false);
        //add the user object
        myObj.add(type, userObj);
        //convert the JSON to string and send back
        out.println(gson.toJson(myObj));
    }

}
