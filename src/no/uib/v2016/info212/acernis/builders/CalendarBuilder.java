package no.uib.v2016.info212.acernis.builders;

import no.uib.v2016.info212.acernis.beans.Schedule;
import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.database.ScheduleHelper;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.util.MyUtils;

import javax.servlet.ServletException;
import java.sql.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Date;

/**
 * Created by stian on 04.10.2016.
 */
public class CalendarBuilder {
    private static SimpleDateFormat sdf = MyUtils.getDateFormatter();

    public static String createCalendar(Connection conn, String context, User user, Calendar month, Locale locale, int monthID) {
        HashMap<String, Schedule> eventList = null;

        StringBuilder calenderCode = new StringBuilder();
        month = (GregorianCalendar) month.clone();
        month.set(Calendar.DATE, 1);
        GregorianCalendar endMonth = (GregorianCalendar) month.clone();
        endMonth.set(Calendar.DAY_OF_MONTH, endMonth.getActualMaximum(Calendar.DAY_OF_MONTH));

        GregorianCalendar nextMonth = (GregorianCalendar) month.clone();
        nextMonth.add(Calendar.MONTH, 1);
        Date start = month.getTime();
        Date end = endMonth.getTime();

        try {
            eventList = ScheduleHelper.getInstance().getSchedule(conn, user.getID(),new java.sql.Date(start.getTime()),new java.sql.Date(end.getTime()));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        // int monthID = month.get(Calendar.MONTH) ;
        int previous = monthID - 1;
        int next = monthID + 1;

        Format titleFmt = new SimpleDateFormat("MMMM yyyy"); //Month Name
        calenderCode.append("<div class=\"table-responsive\">");
        calenderCode.append("<caption>");
        calenderCode.append("<table id=\"schedule-data\" class=\"table table-hover\" style=\"table-layout: fixed; \">\n" +
                " <form id=\"month_control_prev\" action=\"" + context + "/editUser\" method=\"get\">\n"
                + "<input type=\"hidden\" name=\"uid\" value=\"" + user.getID() + "\">\n"
                + "<input type=\"hidden\" name=\"month\" value=\"" + previous + "\">\n"
                + "<a class=\"btn btn-primary btn-md \" style=\"width: 100px;\" href=\"javascript:{}\" onclick=\"document.getElementById('month_control_prev').submit();\" ><i class=\"glyphicon glyphicon-arrow-left\"></i></a>\n"
                + "</form>\n");

        calenderCode.append(titleFmt.format(month.getTime()));
        calenderCode.append("<form id=\"month_control_next\" action=\"" + context + "/editUser\" method=\"get\">\n"
                + "<input type=\"hidden\" name=\"uid\" value=\"" + user.getID() + "\">\n"
                + "<input type=\"hidden\" name=\"month\" value=\"" + next + "\">\n"
                + "<a class=\"btn btn-primary btn-md \" style=\"width: 100px;\" href=\"javascript:{}\" onclick=\"document.getElementById('month_control_next').submit();\" ><i class=\"glyphicon glyphicon-arrow-right\"></i></a>\n"
                + "</form>\n");
        calenderCode.append("</caption>\n");


        // Rewind to the start of the week, then rewind another week
        //  to get the headings
        GregorianCalendar d = (GregorianCalendar) month.clone();
        d.add(Calendar.DATE,
                month.getFirstDayOfWeek() - month.get(Calendar.DAY_OF_WEEK) - 7);

        // week names header
        SimpleDateFormat weekAbbrev = new SimpleDateFormat("E");
        calenderCode.append("<tr>");
        for (int i = 0; i < 7; i++, d.add(Calendar.DATE, 1)) {
            calenderCode.append("<th style=\"width: 14.28%;\">");
            calenderCode.append(d.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, locale));
            calenderCode.append("</th>");
        }
        // hack: the TR for the header will be immediately closed below
        assert d.get(Calendar.DAY_OF_WEEK) == month.getFirstDayOfWeek();

        // body
        SimpleDateFormat dayNum = new SimpleDateFormat("d");
        for (; d.before(nextMonth); d.add(Calendar.DATE, 1)) {
            if (d.get(Calendar.DAY_OF_WEEK) == month.getFirstDayOfWeek()) {
                calenderCode.append("</tr>\n<tr>\n");
            }
            if (d.get(Calendar.MONTH) != month.get(Calendar.MONTH)) {
                calenderCode.append("<td class=\"text-left container w3-card-2 w3-hover-blue\" style=\"\">\n");
                String events = "\n<span id=\"" + String.format("uid=%s&date=%s", user.getID(), sdf.format(d.getTime())) + "\">\n</span>";
                if (events != null) {
                    calenderCode.append(events);
                }
                calenderCode.append("</td>\n");
            } else {
                calenderCode.append("<td style=\"overflow:hidden;\" class=\"text-left container w3-card-2 w3-hover-blue\" style=\"\" ><div>\n");
                calenderCode.append(dayNum.format(d.getTime()));
                calenderCode.append("</div>\n");

                String events = getDayContents(eventList, (GregorianCalendar) d.clone(), user);
                if (events != null) {
                    calenderCode.append(events);
                }

                calenderCode.append("</td>\n");
            }
        }
        calenderCode.append("</tr>\n</table>\n</div>\n");
        return calenderCode.toString();
    }


    protected static int calculateWeeksInYear( Calendar cal) {
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        cal.set(Calendar.DAY_OF_MONTH, 31);

        int ordinalDay = cal.get(Calendar.DAY_OF_YEAR);
        int weekDay = cal.get(Calendar.DAY_OF_WEEK) - 1; // Sunday = 0
        int numberOfWeeks = (ordinalDay - weekDay + 10) / 7;
        return numberOfWeeks;
    }


    /**
     *
     * @param conn
     * @param context
     * @param cal
     * @param locale
     * @param weekNumber
     * @param year
     * @return
     */
    public static String createWeek(Connection conn, int userlevel, String context, Calendar cal, Locale locale, Object weekNumber, Object year) {
        HashMap<String, Schedule> eventList = null;
        List<User> userList = null;

        StringBuilder weekCode = new StringBuilder();

        if (weekNumber == null) {
            weekNumber = cal.get(Calendar.WEEK_OF_YEAR);
        }
        if (year == null) {
            year = cal.get(Calendar.YEAR);
        }

        cal.set(Calendar.YEAR, (int) year);
        cal.set(Calendar.WEEK_OF_YEAR, (int) weekNumber);


        Date yourDate = cal.getTime();

        cal.setTime(yourDate);//Set specific Date of which start and end you want

        Date start, end;

        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        start = cal.getTime();//Date of Monday of current week

        cal.add(Calendar.DATE, 6);//Add 6 days to get Sunday of next week
        cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
        end = cal.getTime();//Date of Sunday of c


        try {
            userList = UserHelper.getInstance().getAllUsers(conn,userlevel);
            Collections.sort(userList,User.COMPARE_BY_NAME);
            //  eventList = helper.getSchedule(conn, sdf.format(start).toString(), sdf.format(end).toString());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ServletException e) {
            e.printStackTrace();
        }
        Format titleFmt = new SimpleDateFormat("MMMM yyyy"); //Month Name
        weekCode.append("<div class=\"table-responsive\">");
        weekCode.append("<table  id=\"schedule-data\" class=\"table table-hover table-striped\" style=\"table-layout: fixed; \">");
        weekCode.append(" <tr>\n" +
                "          <th colspan=\"5\"  class=\"w3-margin-bottom\">  <form id=\"update_schedule\" action=\"" + context + "/schedule\" method=\"get\">\n" +
                "                <label>&nbsp;Week:&nbsp;<input type=\"number\" name=\"weekNumber\" id=\"week\" value=\"" + weekNumber + "\" min=\"1\" max=\"" + calculateWeeksInYear((Calendar)cal.clone()) + "\"></label>\n" +
                "                <label>&nbsp;Year:&nbsp;<input type=\"number\" name=\"year\" id=\"year\" value=\"" + cal.get(Calendar.YEAR) + "\" min=\"" + (Calendar.getInstance().get(Calendar.YEAR) - 10) + "\" max=\"" + (Calendar.getInstance().get(Calendar.YEAR) + 5) + "\"></label>&nbsp;&nbsp;\n" +
                "                <input class=\"btn btn-primary btn-md \" type=\"submit\" value=\"Refresh\">    \n" +
                "            </form>\n" +
                "            </th>\n<th colspan=\"3\" >");
        weekCode.append(titleFmt.format(cal.getTime()) + " Week: " + weekNumber + "</th></tr>");


        // Rewind to the start of the week, then rewind another week
        //  to get the headings
        GregorianCalendar d = (GregorianCalendar) cal.clone();
        d.add(Calendar.DATE,
                cal.getFirstDayOfWeek() - cal.get(Calendar.DAY_OF_WEEK) - 7);

        // week names header
        SimpleDateFormat weekAbbrev = new SimpleDateFormat("E");
        weekCode.append("<tr>");
        weekCode.append("<th style=\"width: 14%;\">Employee</th>");

        for (int i = 0; i < 7; i++, d.add(Calendar.DATE, 1)) {
            weekCode.append("<th style=\"width: 12.5%;\">");
            weekCode.append(d.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, locale));
            weekCode.append("</th>");

        }

        // hack: the TR for the header will be immediately closed below
        assert d.get(Calendar.DAY_OF_WEEK) == cal.getFirstDayOfWeek();


        // body
        SimpleDateFormat dayNum = new SimpleDateFormat("d");
        for (User user : userList) {
            d.setTime(start);
            weekCode.append("<tr>\n");
            weekCode.append(String.format("<td><a href=\"%s/editUser?uid=%s\">%s %s\n<br>(%s)</a>\n</td>", context, user.getID(), user.getfName(), user.getlName(), user.getID()));
            for (int i = 0; i < 7; i++, d.add(Calendar.DATE, 1)) {

                weekCode.append("<td style=\"overflow:hidden;\" class=\"text-left container w3-card-2 w3-hover-blue\" style=\"\" >");

                try {
                    eventList = ScheduleHelper.getInstance().getSchedule(conn, user.getID(), new java.sql.Date(start.getTime()), new java.sql.Date(end.getTime()));
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ServletException e) {
                    e.printStackTrace();
                }


                String events = getDayContents(eventList, (GregorianCalendar) d.clone(), user);
                if (events != null) {
                    weekCode.append(events);
                }

                weekCode.append("</td>\n");
            }

            weekCode.append("</tr>\n");

        }


        weekCode.append("</table>\n</div>\n");
        return weekCode.toString();
    }


    /**
     * @param scheduleHashMap
     * @param day
     * @param user
     * @return
     */
    private static String getDayContents(HashMap<String, Schedule> scheduleHashMap, Calendar day, User user) {

        SimpleDateFormat sdf = MyUtils.getDateFormatter();
        SimpleDateFormat stf = MyUtils.getTimeFormatter();

        if (scheduleHashMap == null) {
            return "\n<span id=\"" + String.format("uid=%s&date=%s", user.getID(), sdf.format(day.getTime())) + "\">\n</span>";
        }
       Schedule today = scheduleHashMap.get(sdf.format(day.getTime()));


        if (today == null) {
           return "\n<span id=\"" + String.format("uid=%s&date=%s", user.getID(), sdf.format(day.getTime())) + "\">\n</span>";
        }
        return "<span id=\"" + String.format("uid=%s&event=%s&date=%s", user.getID(), today.getID(), sdf.format(day.getTime())) + "\">\n" + stf.format(today.getStartWorkTime()) + "-" + stf.format(today.getEndWorkTime()) + "</span>\n";
    }


}
