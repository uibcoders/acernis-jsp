package no.uib.v2016.info212.acernis.servlets;

import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.builders.CalendarBuilder;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.util.MyUtils;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by stian on 26.09.2016.
 */
@WebServlet(urlPatterns = {"/editUser"})
public class EditUserServlet extends HttpServlet {
    static Logger logger = Logger.getLogger(LogoutServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);


        try {
            User user = null;
            Object id = Integer.parseInt(request.getParameter("uid"));

            Locale locale = request.getLocale();
            Calendar cal = Calendar.getInstance(locale);
            int month = cal.get(Calendar.MONTH);

            try {
                month = (Integer.parseInt(request.getParameter("month")));
                cal.set(Calendar.MONTH, month);

            } catch (NullPointerException nex) {

            } catch (NumberFormatException ex) {

            }


            if (id != null) {
                user = UserHelper.getInstance().findUser(conn, (int) id);

                request.setAttribute("htmlCalendar", CalendarBuilder.createCalendar(conn, request.getContextPath(), user, cal, locale, month));

                request.setAttribute("editUser", user);


                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/editUserView.jsp");

                dispatcher.forward(request, response);


            } else {
                response.sendRedirect(request.getContextPath() + "/users");
            }

        } catch (SQLException e) {
            String message = String.format("Error finding user id. Message: %s", e.getMessage());
            logger.error(message);
            response.sendRedirect(request.getContextPath() + "/users");


        } catch (Exception e) {
            response.sendRedirect(request.getContextPath() + "/users");

        }


    }


}
