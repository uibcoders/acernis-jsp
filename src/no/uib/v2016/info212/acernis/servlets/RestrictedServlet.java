package no.uib.v2016.info212.acernis.servlets;

import no.uib.v2016.info212.acernis.beans.Schedule;
import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.database.ScheduleHelper;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.output.Output;
import no.uib.v2016.info212.acernis.parsers.TokenParser;
import no.uib.v2016.info212.acernis.util.KeyStoreUtils;
import no.uib.v2016.info212.acernis.util.MyUtils;
import no.uib.v2016.info212.acernis.util.PasswordUtils;

import javax.crypto.SecretKey;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Time;

/**
 * Created by stian on 15.09.2016.
 */
@WebServlet(urlPatterns = {"/restricted"})
public class RestrictedServlet extends HttpServlet {
    private final String messageNothingToDo = "I could do nothing with that request, please try harder to make it happen!";

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");
        checkRequest(request, out, response);
        out.close();

    }

    private void checkRequest(HttpServletRequest request, PrintWriter out, HttpServletResponse response) {
        boolean isAdmin = false;
        Connection conn = MyUtils.getStoredConnection(request);
        String token = "";
        int currentID = -1;

        try {
            token = request.getHeader("Authorization");
            isAdmin = TokenParser.isAdmin(conn, token);
            currentID = TokenParser.getUserIdFromToken(token);

        } catch (Exception ex) {
            isAdmin = false;
            currentID = -1;
        }

        try {
            if (!isAdmin) {
                token = request.getParameter("token");

                isAdmin = TokenParser.isAdmin(conn, token);
                currentID = TokenParser.getUserIdFromToken(token);
            }

        } catch (Exception ex) {
            isAdmin = false;
            currentID = -1;
        }

        if (isAdmin) {

            response.setStatus(200);

            UserHelper userHelper = UserHelper.getInstance();
            ScheduleHelper scheduleHelper = ScheduleHelper.getInstance();

            String key = null;
            String user = null;
            Object scheduleStatus = null;

            try {
                key = request.getParameter("key").trim();

            } catch (NullPointerException ex) {

            }
            try {
                user = request.getParameter("user").trim();

            } catch (NullPointerException ex) {

            }
            try {
                scheduleStatus = Integer.parseInt(request.getParameter("schedule").trim());

            } catch (NullPointerException ex) {

            }


            if (key != null) {

                if (key.equals("create")) { //check if admin with old key ? Move to another servlet?
                    try {
                        SecretKey tempKey = KeyStoreUtils.generateKey();
                        Output.printValid(out, "createKey", KeyStoreUtils.saveKey(tempKey));

                    } catch (NoSuchAlgorithmException e) {
                        Output.printValid(out, "createKey", e);

                        e.printStackTrace();
                    } catch (IOException e) {
                        Output.printValid(out, "createKey", e);

                        e.printStackTrace();
                    }

                } else {
                    response.setStatus(501);

                    Output.printInvalid(out, "key", messageNothingToDo);

                }
            } else if (user != null) {
                if (user.equals("create")) {
                    try {
                        User tempUser = getUserFromRequest(request);

                        Output.printValid(out, "createUser", userHelper.createUser(conn, tempUser));
                    } catch (SQLException e) {
                        Output.printValid(out, "createUser", e);
                        e.printStackTrace();
                    } catch (ServletException e) {
                        Output.printValid(out, "createUser", e);
                        e.printStackTrace();
                    }

                } else if (user.equals("update")) {
                    try {
                        User tempUser = getUserFromRequest(request);

                        Output.printValid(out, "editUser", userHelper.updateUser(conn, tempUser, token));
                    } catch (SQLException e) {
                        Output.printValid(out, "editUser", e);
                        e.printStackTrace();
                    } catch (ServletException e) {
                        Output.printValid(out, "editUser", e);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Output.printValid(out, "editUser", e);
                        e.printStackTrace();
                    }
                } else if (user.equals("delete")) {

                    try {
                        int deleteID = Integer.parseInt(request.getParameter("uid"));
                        if (deleteID != currentID) {
                            Output.printValid(out, "deleteUser", userHelper.deleteUser(conn, deleteID));
                        } else {
                            Output.printValid(out, "deleteUser", "You cannot delete yourself.");
                        }

                    } catch (SQLException e) {
                        Output.printValid(out, "deleteUser", e);
                    } catch (ServletException e) {
                        Output.printValid(out, "deleteUser", e);
                    }


                } else {
                    response.setStatus(501);
                    Output.printInvalid(out, "user", messageNothingToDo);
                }
            } else if (scheduleStatus != null) {
                if ((int) scheduleStatus == 0) //Create
                {
                    try {
                        Schedule tempSchedule = getScheduleFromRequest(request);
                        Output.printValid(out, "createSchedule", scheduleHelper.createSchedule(conn, tempSchedule));
                    } catch (SQLException e) {
                        Output.printValid(out, "createSchedule", e);
                        e.printStackTrace();
                    } catch (ServletException e) {
                        Output.printValid(out, "createSchedule", e);
                        e.printStackTrace();
                    } catch (Exception e) {
                        Output.printValid(out, "createSchedule", e);
                        e.printStackTrace();                    }

                } else if ((int) scheduleStatus == 1) // Change/Update
                {
                    Output.printValid(out, "schedule", "Will update it if everything is good!");

                } else {
                    response.setStatus(501);
                    Output.printInvalid(out, "schedule", messageNothingToDo);
                }
            } else {
                response.setStatus(501);
                Output.printInvalid(out, "request", messageNothingToDo);
            }
        } else {
            response.setStatus(403);
            out.println("You have strayed off your correct path!");
        }
    }

    /**
     * Gets params from request to create a new user, if params missing it will return a null
     *
     * @param request The servlet request
     * @return A user if params are correct.
     */
    private User getUserFromRequest(HttpServletRequest request) {

        User user = new User();
        boolean changePassword = false;
        int cp = 0;
        try {
            cp = Integer.parseInt(request.getParameter("cp").trim());
            if (cp == 0) {
                changePassword = false;
            } else if (cp == 1) {
                changePassword = true;
            } else {
                changePassword = false;
            }
        } catch (Exception ex) {
            changePassword = false;

        }

        try {
            user.setID(Integer.parseInt(request.getParameter("uid").trim()));

        } catch (NullPointerException nex) {
            user.setID(-1);
        }
        try {
            user.setUserLevel(Integer.parseInt(request.getParameter("ul").trim()));
            user.setUserName(request.getParameter("u").trim());
            if (changePassword) {
                try {
                    user.setOldPassword((request.getParameter("oldp").trim()));
                } catch (Exception e) {

                }

            }
            user.setPassword(PasswordUtils.createHash(request.getParameter("p").trim()));

            user.setfName(request.getParameter("fname").trim());
            user.setlName(request.getParameter("lname").trim());

            return user;


        } catch (Exception ex) {

        }
        return null;

    }

    private Schedule getScheduleFromRequest(HttpServletRequest request) throws Exception {

        Schedule schedule = new Schedule();


        try {
            schedule.setUserID(Integer.parseInt(request.getParameter("uid").trim()));

        } catch (NullPointerException nex) {
            schedule.setUserID(-1);
        }

        try {
            java.util.Date parsed = MyUtils.getDateFormatter().parse(request.getParameter("date").trim());
            Long timeFromParsed = MyUtils.getTimeFormatter().parse(request.getParameter("time-from").trim()).getTime();
            Long timeToParsed = MyUtils.getTimeFormatter().parse(request.getParameter("time-to").trim()).getTime();

            schedule.setWorkDate(new Date(parsed.getTime()));
            schedule.setStartWorkTime(new Time(timeFromParsed));
            schedule.setEndWorkTime(new Time(timeToParsed));

            return schedule;


        } catch (Exception ex) {
            throw new Exception("Error fetching schedule from request"+ ex.getMessage(),ex);

        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }
}
