package no.uib.v2016.info212.acernis.servlets;

/**
 * Created by stian on 07.09.2016.
 */

import no.uib.v2016.info212.acernis.beans.Schedule;
import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.database.ScheduleHelper;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.output.Output;
import no.uib.v2016.info212.acernis.parsers.TokenParser;
import no.uib.v2016.info212.acernis.util.MyUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

@WebServlet(urlPatterns = {"/db"})
public class RequestServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public RequestServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String requestType = null;

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");


        try {
            requestType = request.getParameter("type").trim();

        } catch (NullPointerException ex) {

        }


        if (requestType == null) {
            Output.printInvalid(out, "type", "invalid");
        } else {
            checkRequest(requestType, request, out);
        }

        out.close();
    }


    private void checkRequest(String type, HttpServletRequest request, PrintWriter out) {


        int currentUserID = TokenParser.getUserIdFromToken((request.getHeader("Authorization")));
        if (currentUserID > -1) {
            Connection conn = MyUtils.getStoredConnection(request);

            if (type.trim().equals("userinfo")) {

                try {
                    User user = UserHelper.getInstance().findUser(conn, currentUserID);
                    if (user == null) {
                        Output.printInvalid(out, "user", "invalid");
                    } else {
                        Output.printValid(out, "userinfo", user);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                } catch (ServletException e) {
                    e.printStackTrace();
                }

            } else if (type.trim().equals("schedule")) {

                Object scheduleList = null;
                Object from = request.getParameter("from");
                Object to = request.getParameter("to");
                try {

                    if (from != null && to != null) {
                        scheduleList = ScheduleHelper.getInstance().getSchedule(conn, currentUserID,new Date(MyUtils.getDateFormatter().parse((String)from).getTime()),new Date(MyUtils.getDateFormatter().parse((String)to).getTime()));

                    } else {
                        scheduleList = ScheduleHelper.getInstance().getSchedule(conn, currentUserID);

                    }

                    if (scheduleList == null) {

                        Output.printInvalid(out, "schedule", "invalid");
                    } else {

                        Output.printValid(out, "schedule", scheduleList);
                    }
                } catch (SQLException e) {
                    out.print("in error sql sc " + e.getMessage());
                    e.printStackTrace();
                } catch (Exception e) {
                    out.print("in error ex sc " + e.getMessage());
                    e.printStackTrace();
                }
            } else if (type.trim().equals("check-in")) {

                try {
                    Date cDate = new Date(MyUtils.getDateFormatter().parse(request.getParameter("date").trim()).getTime());

                   Output.printValid(out, type.trim(), ScheduleHelper.getInstance().updateActualTime(conn, cDate, currentUserID, 0));
                } catch (SQLException e) {
                    e.printStackTrace(out);
                } catch (NullPointerException ex) {
                    out.println("Missing Arguments");
                } catch (ServletException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }


            } else if (type.trim().equals("check-out")) {
                String cDate = request.getParameter("date").trim();
                Output.printValid(out,type.trim(),cDate);
               /* try {

                   Output.printValid(out, type.trim(), ScheduleHelper.getInstance().updateActualTime(conn, MyUtils.getDateFormatter().format(MyUtils.getDateFormatter().parse(cDate), currentUserID, 1));
                } catch (SQLException e) {
                    e.printStackTrace(out);
                } catch (NullPointerException ex) {
                    out.println("Missing Arguments");
                } catch (ServletException e) {
                    e.printStackTrace();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
*/

            } else {
                //   out.println("invalid 2");

                Output.printInvalid(out, "request", "invalid");

            }

        } else {
            //out.println("invalid identity");

            Output.printInvalid(out, "request", "invalid");

        }

    }


}
