package no.uib.v2016.info212.acernis.servlets;

/**
 * Created by stian on 25.09.2016.
 */

import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.util.MyUtils;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by stian on 20.09.2016.
 */
@WebServlet(urlPatterns = { "/users"})
public class UserServlet extends HttpServlet {

    static Logger logger = Logger.getLogger(UserServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
doGet(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);



        List<User> userList = new ArrayList<>();
        try {
            userList.addAll(UserHelper.getInstance().getAllUsers(conn, MyUtils.getUserLoggedIn(request.getSession()).getUserLevel()));
            Collections.sort(userList,User.COMPARE_BY_NAME);
        } catch (SQLException e) {
            logger.error(e.getStackTrace());
        }

        request.setAttribute("userList", userList);




        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/allUsersView.jsp");

        dispatcher.forward(request, response);
    }



}
