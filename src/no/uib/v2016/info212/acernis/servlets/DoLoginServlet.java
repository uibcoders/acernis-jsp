package no.uib.v2016.info212.acernis.servlets;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.database.LoginHelper;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.util.KeyStoreUtils;
import no.uib.v2016.info212.acernis.util.MyUtils;
import org.apache.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Date;

/**
 * Created by stian on 12.09.2016.
 * <p>
 * A servlet dedicated to handle login requests
 */
@WebServlet(urlPatterns = {"/dologin"})
public class DoLoginServlet extends HttpServlet {
    final String PREFIX = "(While Logging in) ";

    private Logger logger = Logger.getLogger(DoLoginServlet.class);

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setHeader("Cache-control", "no-cache, no-store");
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Expires", "-1");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type");
        response.setHeader("Access-Control-Max-Age", "86400");


        checkLogin(request, response, out);


        out.close();

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }


    /**
     * Checks database for valid login.
     * Prints a valid token if login is successful
     *
     * @param request
     * @param response
     * @param out
     */
    private void checkLogin(HttpServletRequest request, HttpServletResponse response, PrintWriter out) throws ServletException, IOException {

        //Set
        boolean isMobile = false;
        boolean hasError = false;
        String errorString = null;
        User user = null;


        //Get parameters
        String userName = request.getParameter("u");
        String password = request.getParameter("p");
        String mobile = request.getParameter("m");


        //Check if external application calling
        if (mobile != null) {
            isMobile = mobile.equals("y");
        }


        //Check if Username and password has been applied
        if (userName == null || password == null
                || userName.length() == 0 || password.length() == 0) {
            hasError = true;
            errorString = "Required username and password!";
        } else {
            //Check login
            Connection conn = MyUtils.getStoredConnection(request);

            try {
                //Check if user has correct login
                user = UserHelper.getInstance().findUser(conn, LoginHelper.getInstance().authenticateLogin(conn, userName, password));

                //Check if user password combo was correct
                if (user == null) {
                    hasError = true;
                    errorString = "Username or password invalid";
                }

                //Catch Exceptions
            } catch (SQLException e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();

            } catch (Exception e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();

            }
        }

        // If error, forward to /WEB-INF/views/login.jsp with userdata
        if (hasError) {//Login failed?

            //Log the error
            logger.error(PREFIX + errorString);

            //Check if external application or not
            if (!isMobile) {

                user = new User();
                user.setUserName(userName);
                user.setPassword(password);

                // Store information in request attribute, before forward.
                request.setAttribute("errorString", errorString);
                request.setAttribute("user", user);

                // Forward to /WEB-INF/views/login.jsp
                RequestDispatcher dispatcher //
                        = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

                dispatcher.forward(request, response);

            }
            else {
                //print json data with failed login for external application
                printFailedLogin(out, errorString);
            }
        }

        // If no error, login correct
        // Store user information in Session
        // And redirect to home page or referring page
        else {
            logger.info(PREFIX + errorString);


            if (!isMobile) {
                //Check if user has clearance to admin pages
                if (user.getUserLevel() >= 1) {
                    //create token for user in changing data
                    user.setTOKEN(createJWT(user.getID(), "Acernis", "Scheduler", -1));

                    //Create session from request
                    HttpSession session = request.getSession();
                    //Store the user in the session
                    MyUtils.storeUserLoggedIn(session, user);

                    //check if request has caller
                    String caller = request.getParameter("caller");



                    //Check if caller
                    if (caller != null && caller.length() >= 8) {
                        //Logging caller for debugging
                        logger.info(PREFIX + "We have a caller! " + caller);
                        //Decode caller
                        String uri = new String(Base64.getDecoder().decode(caller), StandardCharsets.UTF_8);

                        //Logging caller for debugging
                        logger.info(PREFIX + "Redirecting to decoded caller: " + uri);

                        //Redirect to caller
                        response.sendRedirect(uri);

                    } else {
                        //Log for debugging
                        logger.info(PREFIX + "Redirecting to /home");

                        //Redirect to home page
                        response.sendRedirect(request.getContextPath() + "/home");
                    }

                } else

                {
                    //Set error
                    errorString = "You don't have clearance!";

                    //Set request attributes for use in jsp
                    request.setAttribute("errorString", errorString);

                    //Log clearance issue for debugging
                    logger.warn(PREFIX + errorString + " " + user.getUserName());

                    //Forward data to try login again
                    RequestDispatcher dispatcher //
                            = this.getServletContext().getRequestDispatcher("/WEB-INF/views/loginView.jsp");

                    dispatcher.forward(request, response);
                }
            } else {
                //Login success from external application

                //Logging data for debugging
                logger.info(PREFIX + "Login Successful from external application!");

                //Print JSON data to external application with JWT
                printSuccessfulLogin(out, createJWT(user.getID(), "Acernis", "Scheduler", -1));
            }
        }

    }


    // method to construct a JWT

    /**
     * method to construct a JWT aka a token
     *
     * @param id        User id
     * @param issuer    Issuer of token
     * @param subject   subject of token
     * @param ttlMillis expiration in ..Millisec
     * @return
     */
    private String createJWT(int id, String issuer, String subject, long ttlMillis) {

        //The JWT signature algorithm we will be using to sign the token
        SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS512;

        long nowMillis = System.currentTimeMillis();
        Date now = new Date(nowMillis);

        //Logging data for debugging
        logger.info(PREFIX + String.format("Creating JWT: DATA - ID: %s, Issued At: %s, Subject: %s, Issuer: %s, Expiration: %s", id, now, subject, issuer, ttlMillis));

        //Let's set the JWT Claims
        JwtBuilder builder = null;
        try {
            builder = Jwts.builder().setId(Integer.toString(id))
                    .setIssuedAt(now)
                    .setSubject(subject)
                    .setIssuer(issuer)
                    .signWith(signatureAlgorithm, KeyStoreUtils.loadKey());
        } catch (Exception e) {
            //Logging error
            logger.error(PREFIX + "Error while creating JWT: ", e);
        }

        //if it has been specified, let's add the expiration
        if (ttlMillis >= 0) {
            long expMillis = nowMillis + ttlMillis;
            Date exp = new Date(expMillis);
            builder.setExpiration(exp);
        }

        //Builds the JWT and serializes it to a compact, URL-safe string
        return builder.compact();
    }

    /**
     * Prints a successful login, json formatted, with auth token
     *
     * @param out   The current out stream (PrintWriter)
     * @param TOKEN The aut token
     */
    private void printSuccessfulLogin(PrintWriter out, String TOKEN) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject myObj = new JsonObject();
        myObj.addProperty("login", "successful");
        myObj.addProperty("token", TOKEN);
        myObj.addProperty("type", "Bearer");


        out.println(gson.toJson(myObj));
    }

    /**
     * Prints a failed login, json formatted
     *
     * @param out The current out stream (PrintWriter)
     */
    private void printFailedLogin(PrintWriter out) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonObject myObj = new JsonObject();
        myObj.addProperty("login", "failed");

        out.println(gson.toJson(myObj));
    }

    /**
     * Prints a failed login, json formatted
     *
     * @param out The current out stream (PrintWriter)
     */
    private void printFailedLogin(PrintWriter out, Object obj) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        JsonElement userObj = gson.toJsonTree(obj);

        JsonObject myObj = new JsonObject();
        myObj.addProperty("login", "Failed");
        myObj.add("error", userObj);

        out.println(gson.toJson(myObj));
    }
}

