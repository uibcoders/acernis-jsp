package no.uib.v2016.info212.acernis.servlets;

import no.uib.v2016.info212.acernis.builders.CalendarBuilder;
import no.uib.v2016.info212.acernis.util.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by stian on 20.09.2016.
 */
@WebServlet(urlPatterns = {"/schedule"})
public class ScheduleServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);


        Object weekNumber = request.getAttribute("weekNumber");
        Object year = request.getAttribute("year");

        Locale locale = request.getLocale();
        Calendar cal = Calendar.getInstance(locale);

        if (weekNumber == null) {
            try {
                weekNumber = Integer.parseInt(request.getParameter("weekNumber"));
            } catch (NullPointerException nex) {

            } catch (NumberFormatException ex) {

            }

            try {
                year = Integer.parseInt(request.getParameter("year"));
            } catch (NullPointerException nex) {

            } catch (NumberFormatException ex) {

            }

        }


        request.setAttribute("htmlSchedule", CalendarBuilder.createWeek(conn,MyUtils.getUserLoggedIn(request.getSession()).getUserLevel(), request.getContextPath(), cal, locale, weekNumber, year));

        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/scheduleView.jsp");

        dispatcher.forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }



}
