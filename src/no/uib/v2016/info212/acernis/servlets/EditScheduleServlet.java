package no.uib.v2016.info212.acernis.servlets;

import no.uib.v2016.info212.acernis.beans.Schedule;
import no.uib.v2016.info212.acernis.database.ScheduleHelper;
import no.uib.v2016.info212.acernis.util.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;

@WebServlet(urlPatterns = {"/editSchedule"})
public class EditScheduleServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private String errorString = "";
    private int scheduleStatus = 0; // 0 = create, 1 = edit

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Connection conn = MyUtils.getStoredConnection(request);


        Object userID = null;
        Object date = null;
        Object scheduleID = null;


        try {
            date = request.getParameter("date");

        } catch (NullPointerException nex) {

        }
        try {
            scheduleID = Integer.parseInt(request.getParameter("event"));

        } catch (NullPointerException nex) {

        } catch (NumberFormatException num) {
//do nothing
        }

        try {
            userID = Integer.parseInt(request.getParameter("uid"));

        } catch (NullPointerException nex) {

        } catch (NumberFormatException num) {
//do nothing

        }

        if (userID != null) {
            request.setAttribute("editSchedule", showDaySchedule(conn, scheduleID, date, userID));

        } else {
            errorString = "No user specified!";

        }
        request.setAttribute("scheduleStatus", scheduleStatus);

        request.setAttribute("errorString", errorString);


        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/WEB-INF/views/editScheduleView.jsp");

        dispatcher.forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }


    /**
     * @param conn
     * @param scheduleID
     * @param date
     * @param userID
     * @return
     */
    private Schedule showDaySchedule(Connection conn, Object scheduleID, Object date, Object userID) {
        try {

            if (scheduleID != null) {
                scheduleStatus = 1;
                return ScheduleHelper.getInstance().getDaySchedule(conn, (int) userID, (int) scheduleID);

            } else {
                scheduleStatus = 0;

                Schedule newSchedule = new Schedule();
                newSchedule.setUserID((int) userID);

                if (date != null) {
                    try {
                        newSchedule.setWorkDate(new Date(MyUtils.getDateFormatter().parse((String)date).getTime()));

                    } catch (Exception ex) {
                        errorString = "Date was malformed. Please check!";
                    }

                } else {
                    errorString = "No date was specified!, Please Specify.";
                }
                return newSchedule;

            }

        } catch (SQLException e) {
            errorString = "SQL error when getting day Schedule!";

            e.printStackTrace();
        } catch (ServletException e) {
            errorString = "Error! If support, check log.";
            e.printStackTrace();
        }
        return null;
    }
}