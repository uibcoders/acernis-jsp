package no.uib.v2016.info212.acernis.database;

import no.uib.v2016.info212.acernis.beans.User;
import no.uib.v2016.info212.acernis.parsers.TokenParser;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by stian on 04.10.2016.
 */
public class UserHelper {

    PreparedStatement stmt = null;
    String sql = null;
    static UserHelper userHelper = null;
    static Logger logger = Logger.getLogger(UserHelper.class);


    public static UserHelper getInstance() {
        if (userHelper == null) {
            userHelper = new UserHelper();
        }
        return userHelper;
    }


    public int countUsers(Connection conn) throws SQLException, ServletException {

        sql = "Select user_level AS LEVEL, COUNT(*) AS COUNT from Users WHERE user_level = 1";

        try {
            stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                return rs.getInt("COUNT");
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return 0;
    }


    public List<User> getAllUsers(Connection conn) throws SQLException, ServletException {


        User user = null;
        List<User> userList = new ArrayList<>();

        sql = "Select ID,username,user_level, IfNull(first_name, '') AS first_name,IfNull(last_name, '') AS last_name from Users";

        try {
            stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setID(rs.getInt("ID"));
                user.setUserName(rs.getString("username").trim());
                user.setUserLevel(rs.getInt("user_level"));
                user.setfName(rs.getString("first_name"));
                user.setlName(rs.getString("last_name"));
                userList.add(user);


            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return userList;
    }

    public List<User> getAllUsers(Connection conn,int userLevel) throws SQLException, ServletException {


        User user = null;
        List<User> userList = new ArrayList<>();

        sql = "Select ID,username,user_level, IfNull(first_name, '') AS first_name,IfNull(last_name, '') AS last_name from Users WHERE user_level < '" + userLevel + "'";

        try {
            stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setID(rs.getInt("ID"));
                user.setUserName(rs.getString("username").trim());
                user.setUserLevel(rs.getInt("user_level"));
                user.setfName(rs.getString("first_name"));
                user.setlName(rs.getString("last_name"));
                userList.add(user);


            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return userList;
    }

    /**
     * Gets user information from the database
     * TESTING
     *
     * @param userID The user id to find
     * @return A User object with all information obtained
     * @throws SQLException
     */
    public User findUser(Connection conn, int userID) throws SQLException, ServletException {


        User user = null;

        sql = "Select ID,username,user_level, IfNull(first_name, '') AS first_name,IfNull(last_name, '') AS last_name from Users where ID = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = new User();
                user.setID(rs.getInt("ID"));
                user.setUserName(rs.getString("username").trim());
                user.setUserLevel(rs.getInt("user_level"));
                user.setfName(rs.getString("first_name"));
                user.setlName(rs.getString("last_name"));
                return user;

            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return null;
    }


    public User findUser(Connection conn, String userName) throws SQLException, ServletException {

        String sql = "Select a.username, a.password, a.ID from Users a " + " where a.username = ? ";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, userName);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                int ID = rs.getInt("ID");
                //String password = rs.getString("password");
                User user = new User();
                user.setID(ID);
                user.setUserName(userName);
                //user.setPassword(password);
                return user;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return null;
    }


    /**
     * @param user
     * @return
     * @throws SQLException
     */
    public boolean createUser(Connection conn, User user) throws SQLException, ServletException {

        if (user.getUserName().length() < 4) {
            logger.error("'" + user.getUserName() + "'" + "Username not long enough.");

            throw new ServletException("Username not long enough.");
        }
        if (user.getPassword().length() < 4) {
            logger.error("Password not long enough.");

            throw new ServletException("Password not long enough.");
        }
        if (findUser(conn, user.getUserName()) != null) {
            logger.error("User exist " + user.getUserName());

            throw new ServletException("Username already exist!");
        }


        sql = (" INSERT INTO Users "
                + "(ID,username,password,first_name,last_name,user_level)"
                + " VALUES (null, ?, ?, ?, ?, ?)");
        try {

            stmt = conn.prepareStatement(sql);
            stmt.setString(1, user.getUserName());
            stmt.setString(2, user.getPassword());
            stmt.setString(3, user.getfName());
            stmt.setString(4, user.getlName());
            stmt.setInt(5, user.getUserLevel());

            // execute the preparedstatement
            return stmt.executeUpdate() >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem. " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement " + e.getMessage());
            }
        }

    }

    /**
     * @param conn
     * @param userID
     * @return
     * @throws SQLException
     * @throws ServletException
     */
    public boolean deleteUser(Connection conn, int userID) throws SQLException, ServletException {

        if (findUser(conn, userID) != null) {
            logger.error("User exist ID: " + userID);
        } else {
            logger.error("User does not exist ID: " + userID);

            throw new ServletException("User id does not exist!");
        }


        sql = ("DELETE FROM Users WHERE ID = ?");
        try {

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);


            // execute the preparedstatement
            return stmt.executeUpdate() >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem. " + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement " + e.getMessage());
            }
        }

    }

    public boolean updateUser(Connection conn, User user,String token) throws Exception {
        logger.error("Trying to update user, " + user.getUserName());

        if (user.getID() >= 0) {

            if (findUser(conn, user.getUserName()).getID() == user.getID() && user.getPassword() != null) {

                logger.error("User exist and password is not null, Updating Information! " + user.getUserName());
                if (user.getPassword().isEmpty()) {

                    sql = (" UPDATE Users  "
                            + "SET first_name = ?, last_name = ?"
                            + "  WHERE ID = ? AND username = ?");
                } else {
                    boolean isAdmin =  TokenParser.isAdmin(conn,token);

                    sql = (" UPDATE Users  "
                            + "SET first_name = ?, last_name = ?, password = '" + user.getPassword()
                            + "'  WHERE ID = ? AND username = ?");

                    if(user.getOldPassword().isEmpty() && !isAdmin) {

                        logger.error("Old password empty, level not high enough to change without!");
                        throw new ServletException("Old password empty, level not high enough to change without!");
                    }
                    else if (!user.getOldPassword().isEmpty())
                    {
                       if( LoginHelper.getInstance().authenticateLogin(conn,user.getUserName(),user.getOldPassword()) == -1)
                       {
                           logger.error("Old password wrong!!");
                           throw new ServletException("Old password wrong!");
                       }

                    }

                }

                try {
                    stmt = conn.prepareStatement(sql);
                    stmt.setString(1, user.getfName());
                    stmt.setString(2, user.getlName());
                    stmt.setInt(3, user.getID());
                    stmt.setString(4, user.getUserName());

                    // execute the preparedstatement

                    return stmt.executeUpdate() >= 1;
                } catch (SQLException e) {
                    e.printStackTrace();
                    logger.error("Database connection problem. User Update", e);
                    throw new ServletException("DB Connection problem. User Update");
                } finally {
                    try {
                        stmt.close();
                    } catch (SQLException e) {
                        logger.error("SQLException in closing PreparedStatement", e);
                    }
                }

            }
        }
        logger.error("Could not update user with ID: " + user.getID() + ", Name: " + user.getUserName());

        return false;
    }


}
