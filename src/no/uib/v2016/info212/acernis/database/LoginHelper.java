package no.uib.v2016.info212.acernis.database;

import no.uib.v2016.info212.acernis.util.PasswordUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


/**
 * Created by stian on 08.09.2016.
 */
public class LoginHelper {

    PreparedStatement stmt = null;
    String sql = null;
    static LoginHelper loginHelper = null;
    static Logger logger = Logger.getLogger(LoginHelper.class);


    public static LoginHelper getInstance() {
        if (loginHelper == null) {
            loginHelper = new LoginHelper();
        }
        return loginHelper;
    }

    /**
     * NOT COMPLETE!"!
     *
     * @param strUserName Username
     * @param strPassword Password
     * @return If accepted
     * @throws Exception
     */
    public int authenticateLogin(Connection conn, String strUserName, String strPassword) throws Exception {
        int userID = -1;
        String argPass = null;


        try {
            stmt = conn.prepareStatement("SELECT ID, username,password FROM Users WHERE username = ? ");
            stmt.setString(1, strUserName);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                argPass = rs.getString("password");
                try {
                    if (PasswordUtils.verifyPassword(strPassword, argPass)) {
                        userID = rs.getInt("ID");
                        logger.info(userID + " login is valid in DB");
                    }
                } catch (Exception ex) {
                    userID = -1;
                    throw new Exception("Error While Validating User");
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem. Auth");
            throw new ServletException("DB Connection problem. Auth");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return userID;
    }




}
