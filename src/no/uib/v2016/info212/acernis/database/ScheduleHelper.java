package no.uib.v2016.info212.acernis.database;

import no.uib.v2016.info212.acernis.beans.Schedule;
import no.uib.v2016.info212.acernis.util.MyUtils;
import org.apache.log4j.Logger;

import javax.servlet.ServletException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by stian on 04.10.2016.
 */
public class ScheduleHelper {

    PreparedStatement stmt = null;
    String sql = null;
    static ScheduleHelper scheduleHelper = null;
    static Logger logger = Logger.getLogger(ScheduleHelper.class);

    public static ScheduleHelper getInstance() {
        if (scheduleHelper == null) {
            scheduleHelper = new ScheduleHelper();
        }
        return scheduleHelper;
    }


    public List<Schedule> getSchedule(Connection conn, int userID) throws SQLException, ServletException {
        List<Schedule> schedule_list = new ArrayList<>();
        Schedule schedule = null;


        sql = "SELECT Schedules.ID AS SID, Schedules.user_id, Schedules.work_date, Schedules.start_work_time, Schedules.end_work_time, Schedules.last_update AS schedule_last_update,Timestamps.ID AS TSID,Timestamps.check_in,Timestamps.check_out FROM Schedules LEFT JOIN Timestamps" +
                " ON Schedules.work_date=Timestamps.work_date AND Schedules.user_id=Timestamps.user_id where Schedules.user_id = ?";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                schedule = new Schedule();
                schedule.setID(rs.getInt("SID"));
                schedule.setUserID(rs.getInt("user_id"));
                schedule.setWorkDate(rs.getDate("work_date"));
                schedule.setStartWorkTime(rs.getTime("start_work_time"));
                schedule.setEndWorkTime(rs.getTime("end_work_time"));
                Time actual_start = rs.getTime("check_in");
                if (rs.wasNull()) actual_start = new Time(0);
                schedule.setCheckIN(actual_start);
                Time actual_end = rs.getTime("check_out");
                if (rs.wasNull()) actual_end = new Time(0);
                schedule.setCheckOUT(actual_end);
                int timeStampId = rs.getInt("TSID");
                if (rs.wasNull()) timeStampId = 0;
                schedule.setTSID(timeStampId);

                schedule_list.add(schedule);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return schedule_list;
    }

    public HashMap<String, Schedule> getSchedule(Connection conn, int userID, Date startDate, Date endDate) throws SQLException, ServletException {
        HashMap<String, Schedule> schedule_list = new HashMap<>();
        Schedule schedule = null;

        sql = "SELECT Schedules.ID AS SID, Schedules.user_id, Schedules.work_date, Schedules.start_work_time, Schedules.end_work_time, Schedules.last_update AS schedule_last_update,Timestamps.ID AS TSID,Timestamps.check_in,Timestamps.check_out FROM Schedules LEFT JOIN Timestamps" +
                " ON Schedules.work_date=Timestamps.work_date AND Schedules.user_id=Timestamps.user_id  WHERE Schedules.user_id = ? AND Schedules.work_date BETWEEN ? AND ?";

        // sql = "SELECT u.ID,u.username, schedules.* FROM Users AS u " +
        //       " INNER JOIN ( SELECT ID as SID,user_id,work_date,start_work_time,end_work_time,actual_start_time,actual_end_time FROM Schedules WHERE work_date BETWEEN ? AND ?) AS schedules " +
        //     " ON u.ID = schedules.user_id";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            stmt.setDate(2, startDate);
            stmt.setDate(3, endDate);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                schedule = new Schedule();
                schedule.setID(rs.getInt("SID"));
                schedule.setUserID(rs.getInt("user_id"));
                schedule.setWorkDate(rs.getDate("work_date"));
                schedule.setStartWorkTime(rs.getTime("start_work_time"));
                schedule.setEndWorkTime(rs.getTime("end_work_time"));
                Time actual_start = rs.getTime("check_in");
                if (rs.wasNull()) actual_start = new Time(0);
                schedule.setCheckIN(actual_start);
                Time actual_end = rs.getTime("check_out");
                if (rs.wasNull()) actual_end = new Time(0);
                schedule.setCheckOUT(actual_end);
                int timeStampId = rs.getInt("TSID");
                if (rs.wasNull()) timeStampId = 0;
                schedule.setTSID(timeStampId);

                schedule_list.put(MyUtils.getDateFormatter().format(schedule.getWorkDate()), schedule);
            }



        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return schedule_list;
    }

    public List<Schedule> getSchedule(Connection conn, Date startDate, Date endDate) throws SQLException, ServletException {
        List<Schedule> schedule_list = new ArrayList<>();
        Schedule schedule = null;

        sql = "SELECT Schedules.ID AS SID, Schedules.user_id, Schedules.work_date, Schedules.start_work_time, Schedules.end_work_time, Schedules.last_update AS schedule_last_update,Timestamps.ID AS TSID,Timestamps.check_in,Timestamps.check_out FROM Schedules LEFT JOIN Timestamps" +
                " ON Schedules.work_date=Timestamps.work_date AND Schedules.user_id=Timestamps.user_id  WHERE Schedules.work_date BETWEEN ? AND ?";

        // sql = "SELECT u.ID,u.username, schedules.* FROM Users AS u " +
        //       " INNER JOIN ( SELECT ID as SID,user_id,work_date,start_work_time,end_work_time,actual_start_time,actual_end_time FROM Schedules WHERE work_date BETWEEN ? AND ?) AS schedules " +
        //     " ON u.ID = schedules.user_id";
        try {
            stmt = conn.prepareStatement(sql);
            stmt.setDate(1, startDate);
            stmt.setDate(2, endDate);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                schedule = new Schedule();
                schedule.setID(rs.getInt("SID"));
                schedule.setUserID(rs.getInt("user_id"));
                schedule.setWorkDate(rs.getDate("work_date"));
                schedule.setStartWorkTime(rs.getTime("start_work_time"));
                schedule.setEndWorkTime(rs.getTime("end_work_time"));
                Time actual_start = rs.getTime("check_in");
                if (rs.wasNull()) actual_start = new Time(0);
                schedule.setCheckIN(actual_start);
                Time actual_end = rs.getTime("check_out");
                if (rs.wasNull()) actual_end = new Time(0);
                schedule.setCheckOUT(actual_end);
                int timeStampId = rs.getInt("TSID");
                if (rs.wasNull()) timeStampId = 0;
                schedule.setTSID(timeStampId);

                schedule_list.add(schedule);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return schedule_list;
    }


    public Schedule getDaySchedule(Connection conn, int userID, Date date) throws SQLException, ServletException {
        Schedule schedule = null;

        sql = "SELECT Schedules.ID AS SID, Schedules.user_id, Schedules.work_date, Schedules.start_work_time, Schedules.end_work_time, Schedules.last_update AS schedule_last_update,Timestamps.ID AS TSID,Timestamps.check_in,Timestamps.check_out FROM Schedules LEFT JOIN Timestamps" +
                " ON Schedules.work_date=Timestamps.work_date AND Schedules.user_id=Timestamps.user_id  WHERE Schedules.user_id = ? AND Schedules.work_date = ?";


        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            stmt.setDate(2, date);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                schedule = new Schedule();
                schedule.setID(rs.getInt("SID"));
                schedule.setUserID(rs.getInt("user_id"));
                schedule.setWorkDate(rs.getDate("work_date"));
                schedule.setStartWorkTime(rs.getTime("start_work_time"));
                schedule.setEndWorkTime(rs.getTime("end_work_time"));
                Time actual_start = rs.getTime("check_in");
                if (rs.wasNull()) actual_start = new Time(0);
                schedule.setCheckIN(actual_start);
                Time actual_end = rs.getTime("check_out");
                if (rs.wasNull()) actual_end = new Time(0);
                schedule.setCheckOUT(actual_end);
                int timeStampId = rs.getInt("TSID");
                if (rs.wasNull()) timeStampId = 0;
                schedule.setTSID(timeStampId);

            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return schedule;
    }


    public Schedule getDaySchedule(Connection conn, int userID, int scheduleID) throws SQLException, ServletException {
        Schedule schedule = null;

        sql = "SELECT Schedules.ID AS SID, Schedules.user_id, Schedules.work_date, Schedules.start_work_time, Schedules.end_work_time, Schedules.last_update AS schedule_last_update,Timestamps.ID AS TSID,Timestamps.check_in,Timestamps.check_out FROM Schedules LEFT JOIN Timestamps" +
                " ON Schedules.work_date=Timestamps.work_date AND Schedules.user_id=Timestamps.user_id  WHERE Schedules.user_id = ? AND Schedules.ID = ?";


        try {
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userID);
            stmt.setInt(2, scheduleID);

            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                schedule = new Schedule();
                schedule.setID(rs.getInt("SID"));
                schedule.setUserID(rs.getInt("user_id"));
                schedule.setWorkDate(rs.getDate("work_date"));
                schedule.setStartWorkTime(rs.getTime("start_work_time"));
                schedule.setEndWorkTime(rs.getTime("end_work_time"));
                Time actual_start = rs.getTime("check_in");
                if (rs.wasNull()) actual_start = new Time(0);
                schedule.setCheckIN(actual_start);
                Time actual_end = rs.getTime("check_out");
                if (rs.wasNull()) actual_end = new Time(0);
                schedule.setCheckOUT(actual_end);
                int timeStampId = rs.getInt("TSID");
                if (rs.wasNull()) timeStampId = 0;
                schedule.setTSID(timeStampId);


            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem");
            throw new ServletException("DB Connection problem.");
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement");
            }
        }
        return schedule;
    }


    /**
     * @param workDate
     * @param userID
     * @param actualTimeValue 0 = starttime, 1 = endtime
     * @return
     * @throws SQLException
     */
    public boolean updateActualTime(Connection conn, Date workDate, int userID, int actualTimeValue) throws SQLException, ServletException {

        if (actualTimeValue == 0 || actualTimeValue == 1) {
            String[] actualTime = {"check_in", "check_out"};

            java.util.Date today = new java.util.Date();
            Time now = new Time(today.getTime());

            sql = ("INSERT INTO `Timestamps`(`ID`, `user_id`, `work_date`, `check_in`, `check_out`, `last_update`) VALUES (NULL,?,?,?,?,NULL) ON DUPLICATE KEY UPDATE " + actualTime[actualTimeValue] + "=? WHERE " + actualTime[actualTimeValue] + " IS NULL");


            try {
                stmt = conn.prepareStatement(sql);
                stmt.setInt(1, userID);
                stmt.setString(2, MyUtils.getDateFormatter().format(workDate));
                if (actualTimeValue == 0) {
                    stmt.setTime(3, now);
                    stmt.setTime(4, null);

                } else if (actualTimeValue == 1) {
                    stmt.setTime(3, null);
                    stmt.setTime(4, now);
                }

                stmt.setTime(5, now);

                // execute the preparedstatement

                return stmt.executeUpdate() >= 1;
            } catch (SQLException e) {
                e.printStackTrace();
                logger.error("Database connection problem", e);
                throw new ServletException("DB Connection problem.");
            } finally {
                try {
                    stmt.close();
                } catch (SQLException e) {
                    logger.error("SQLException in closing PreparedStatement",e);
                }
            }

        }
        return false;
    }


    public boolean createSchedule(Connection conn, Schedule schedule) throws SQLException, ServletException {


        //TODO: Check userid

        if (schedule.getUserID() <= 0) {
            logger.error("Userid not set.");
            throw new ServletException("Cannot create schedule without proper userid");
        }

        if (schedule.getWorkDate() == null) {
            logger.error("Workdate is null.");

            throw new ServletException("Workdate is null.");
        }

        if (schedule.getStartWorkTime() == null) {
            logger.error("StartTime is null.");

            throw new ServletException("Starttime is null.");
        }

        if (schedule.getEndWorkTime() == null) {
            logger.error("EndTime is null.");

            throw new ServletException("EndTime is null.");
        }

        sql = (" INSERT INTO Schedules "
                + " (ID, user_id, work_date, start_work_time, end_work_time, last_update)"
                + " VALUES (NULL , ?, ?, ?, ?, NULL)");
        try {

            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, schedule.getUserID());
            stmt.setDate(2, schedule.getWorkDate());
            stmt.setTime(3, schedule.getStartWorkTime());
            stmt.setTime(4, schedule.getEndWorkTime());

            // execute the preparedstatement
            return stmt.executeUpdate() >= 1;
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("Database connection problem. CreateSchedule");
            throw new ServletException("DB Connection problem. CreateSchedule" + e.getMessage());
        } finally {
            try {
                stmt.close();
            } catch (SQLException e) {
                logger.error("SQLException in closing PreparedStatement " + e.getMessage());
            }
        }

    }
}
