package no.uib.v2016.info212.acernis.beans;

import java.util.Comparator;

/**
 * Created by stian on 07.09.2016.
 */
public class User {

    int ID = -1;
    int userLevel = 1;
    String userName = "";
    String password = "";
    String oldPassword = "";

    String fName = "";
    String lName = "";
    String TOKEN = "";

    public User() {

    }

    public User(int ID, String userName, String password) {
        setID(ID);
        setUserName(userName);
        setPassword(password);

    }

    public User(int ID, int userLevel, String userName, String password, String fName, String lName) {

        setID(ID);
        setUserLevel(userLevel);
        setUserName(userName);
        setPassword(password);
        setfName(fName);
        setlName(lName);
    }

    public static Comparator<User> COMPARE_BY_NAME = (one, other) -> (one.fName + " " + one.lName).compareTo((other.fName + " " + other.lName));
    public static Comparator<User> COMPARE_BY_USERNAME = (one, other) -> one.userName.compareTo(other.userName);
    public static Comparator<User> COMPARE_BY_ID = (one, other) -> one.ID - (other.ID);
    public static Comparator<User> COMPARE_BY_USERLEVEL = (one, other) -> one.userLevel - (other.userLevel);



    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }


    public int getUserLevel() {
        return userLevel;
    }

    public void setUserLevel(int userLevel) {
        this.userLevel = userLevel;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public String getlName() {
        return lName;
    }

    public void setlName(String lName) {
        this.lName = lName;
    }

    public String getTOKEN() {
        return TOKEN;
    }

    public void setTOKEN(String TOKEN) {
        this.TOKEN = TOKEN;
    }



}