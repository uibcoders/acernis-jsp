package no.uib.v2016.info212.acernis.beans;


import java.sql.Time;
import java.sql.Date;

/**
 * Created by stian on 08.09.2016.
 */
public class Schedule {

    /*JSON DATA [ "ID": 40,
      "userID": 1246,
      "TSID": 0,
      "workDate": "2016-09-14",
      "startWorkTime": "09:04:00",
      "endWorkTime": "18:50:00",
      "checkIN": "",
      "checkOUT": ""
    }]

     */

    private int ID;
    private int userID;
    private int TSID;
    private Date workDate = null;
    private Time startWorkTime = null;
    private Time endWorkTime = null;
    private Time checkIN = null;
    private Time checkOUT = null;


    public Schedule() {

    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getTSID() {
        return TSID;
    }

    public void setTSID(int TSID) {
        this.TSID = TSID;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public Date getWorkDate() {
        return workDate;
    }

    public void setWorkDate(Date workDate) {
        this.workDate = workDate;
    }

    public Time getStartWorkTime() {
        return startWorkTime;
    }

    public void setStartWorkTime(Time startWorkTime) {
        this.startWorkTime = startWorkTime;
    }

    public Time getEndWorkTime() {
        return endWorkTime;
    }

    public void setEndWorkTime(Time endWorkTime) {
        this.endWorkTime = endWorkTime;
    }

    public Time getCheckIN() {
        return checkIN;
    }

    public void setCheckIN(Time checkIN) {
        this.checkIN = checkIN;
    }

    public Time getCheckOUT() {
        return checkOUT;
    }

    public void setCheckOUT(Time checkOUT) {
        this.checkOUT = checkOUT;
    }


}
