package no.uib.v2016.info212.acernis.util;

import no.uib.v2016.info212.acernis.beans.User;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;


public class MyUtils {

    public static final String ATT_NAME_CONNECTION = "DBConnection";

    private static final String ATT_NAME_USER_NAME = "ATTRIBUTE_FOR_STORE_USER_NAME_IN_COOKIE";

    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

    private static SimpleDateFormat stf = new SimpleDateFormat("hh:mm");


    public static SimpleDateFormat getDateFormatter()
    {
        return sdf;
    }
    public static SimpleDateFormat getTimeFormatter()
    {
        return stf;
    }

    // Store Connection in request attribute.
    // (Information stored only exist during requests)
    public static void storeConnection(ServletRequest request, Connection conn) {
        request.setAttribute(ATT_NAME_CONNECTION, conn);
    }

    // Get the Connection object has been stored in one attribute of the request.
    public static Connection getStoredConnection(ServletRequest request) {
       // Connection conn = (Connection)  request.getServletContext().getAttribute(ATT_NAME_CONNECTION);;
        try {
            return ConnectionUtils.getInstance().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return  MyUtils.getStoredConnection(request);
    }

    // Store user info in Session.
    public static void storeUserLoggedIn(HttpSession session, User userLoggedIn) {

        // On the JSP can access ${userLoggedIn}
        session.setAttribute("userLoggedIn", userLoggedIn);
    }


    // Get the user information stored in the session.
    public static User getUserLoggedIn(HttpSession session) {
        User userLoggedIn = (User) session.getAttribute("userLoggedIn");
        return userLoggedIn;
    }


    // Store info in Cookie
    public static void storeUserCookie(HttpServletResponse response, User user) {
        System.out.println("Store user cookie");
        Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, user.getUserName());

        // 1 day (Convert to seconds)
        cookieUserName.setMaxAge(24 * 60 * 60);
        response.addCookie(cookieUserName);
    }

    public static String getUserNameInCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (ATT_NAME_USER_NAME.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }


    // Delete cookie.
    public static void deleteUserCookie(HttpServletResponse response) {
        Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, null);

        // 0 seconds (Expires immediately)
        cookieUserName.setMaxAge(0);
        response.addCookie(cookieUserName);
    }

}