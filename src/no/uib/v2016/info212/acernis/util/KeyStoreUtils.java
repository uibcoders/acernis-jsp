package no.uib.v2016.info212.acernis.util;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.FileUtils;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;


public class KeyStoreUtils {

    final static String keyStoreFile = "tokenK.key";

    private static final String ALGO = "AES";
    private static final int KEYSZ = 256;// 128 default; 192 and 256 also possible

    public static SecretKey generateKey() throws NoSuchAlgorithmException
    {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(ALGO);
        keyGenerator.init(KEYSZ);
        SecretKey key = keyGenerator.generateKey() ;
        return key;
    }

    public static boolean saveKey(SecretKey key) throws IOException
    {
        File file = new File(keyStoreFile);

        byte[] encoded = key.getEncoded();
        char[] hex = Hex.encodeHex(encoded);
        String data = String.valueOf(hex);
        FileUtils.writeStringToFile(file, data);
        return true;
    }

    public static SecretKey loadKey() throws IOException, NoSuchAlgorithmException {
        File file = new File(keyStoreFile);

        if(!file.exists()) { saveKey(generateKey());} //create key if never created before

            String data = new String(FileUtils.readFileToByteArray(file));
            char[] hex = data.toCharArray();
            byte[] encoded;
            try {
                encoded = Hex.decodeHex(hex);
            } catch (DecoderException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                return null;
            }

        SecretKey key = new SecretKeySpec(encoded, ALGO);

        return key;
    }
}