package no.uib.v2016.info212.acernis.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
    private Connection connection;

    private String dbURL, user, pwd;
    private static ConnectionUtils connectionUtils;

    public static ConnectionUtils getInstance() {
        if (connectionUtils == null) {
            connectionUtils = new ConnectionUtils();
        }
        return connectionUtils;
    }



    public ConnectionUtils() {

    }

    public void connect(String dbURL, String user, String pwd) throws ClassNotFoundException, SQLException {

       // this.connection = DriverManager.getConnection(dbURL, user, pwd);
        this.dbURL = dbURL;
        this.user = user;
        this.pwd = pwd;
    }

    public Connection getConnection() throws SQLException, ClassNotFoundException {
        Class.forName("com.mysql.jdbc.Driver");
        return DriverManager.getConnection(dbURL, user, pwd);
    }
}