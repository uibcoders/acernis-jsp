package no.uib.v2016.info212.acernis.parsers;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import no.uib.v2016.info212.acernis.database.UserHelper;
import no.uib.v2016.info212.acernis.util.KeyStoreUtils;

import java.sql.Connection;

/**
 * Created by stian on 16.09.2016.
 */
public class TokenParser {


    public static String checkAuthorization(String auth) {
        if (auth.startsWith("Bearer")) {
            return auth.replace("Bearer", "").trim();
        }
        return auth;
    }

    /**
     * Gets a user id from a valid token
     *
     * @param auth The token to be parsed
     * @return A positive user id if correct
     */
    public static int getUserIdFromToken(String auth) {
        //This line will throw an exception if it is not a signed JWS (as expected)

        try {
            if (auth != null ) {
                String token = checkAuthorization(auth);
                if (token != null) {

                    Claims claims = Jwts.parser()
                            .setSigningKey(KeyStoreUtils.loadKey())
                            .parseClaimsJws(token).getBody();

                    return Integer.parseInt(claims.getId());
                }
            }else
            {
                System.out.println("Error checking admin, token null!");
            }
        } catch (Exception e) {
            e.printStackTrace(System.out);
        }
        return -1;
    }

    /**
     * Checks token for user id, then checks if id is admin level
     *
     * @param auth The users token key
     * @return If admin
     */
    public static boolean isAdmin(Connection conn, String auth) {

        //This line will throw an exception if it is not a signed JWS (as expected)
if (conn == null)
{
    System.out.println("Error checking admin, DB connection null!");
    return false;
}
        try {
            if (auth != null) {

                String token = checkAuthorization(auth);
                if (token != null) {
                    int userLevel = UserHelper.getInstance().findUser(conn, getUserIdFromToken(token)).getUserLevel();

                    return userLevel >= 2;
                }
            }
            else
            {
                System.out.println("Error checking admin, token null!");
            }
        } catch (Exception e) {

            e.printStackTrace(System.out);
        }
        return false;
    }
}
